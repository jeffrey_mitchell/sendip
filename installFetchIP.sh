#!/bin/bash


#install the packages
sudo apt-get update
sudo apt-get -y install ssmtp mailutils mpack

#Install as service to run after bootup

cp fetchIP.sh /etc/init.d/fetchIP.sh

chmod 755 /etc/init.d/fetchIP.sh

touch /etc/init.d/runFetchIP

echo "#!/bin/bash -e" > /etc/init.d/runFetchIP
echo "sh '/etc/init.d/fetchIP.sh'" >> /etc/init.d/runFetchIP
echo "exit 0" >> /etc/init.d/runFetchIP

chmod 755 /etc/init.d/runFetchIP

#update the service list, ignore the errors
update-rc.d runFetchIP defaults
