THE PI MUST BE CONNECTED TO THE INTERNET TO PERFORM THESE STEPS

git clone https://URL

1. Go to google and create an application specific password by following these directions

https://support.google.com/accounts/answer/185833?hl=en
*One important thing to note. Well there are spaces shown by google when they present the password there should be no spaces when using it. (Remove the spaces if manually typing)

2. Run the install script

cd /sendIP (where you did the git clone)
First we edit fetchIP.sh, grab your fav text editor (I am using vim however nano/pico will work too)
vim fetchIP.sh
Edit the ENTER_YOUR_EMAIL@gmail.com with your email (do not adjust spacing off the equal sign and leave @gmail.com)

Now we install:
sudo sh installFetchIP.sh

This will take a while since there are a few large packages that need to be installed, we will get an error at the end that reads
insserv: warning: ..... (ignore this)

3. Edit the ssmtp file

sudo vim pico /etc/ssmtp/ssmtp.conf


mailhub=smtp.gmail.com:587 (already in file just change the value)
hostname=ENTER YOUR PI HOST (should already be there so probably just leave this alone)
**Add these**
AuthUser=YOU@gmail.com
AuthPass=PASSWORD (USE THE APPLICATION SPEC PASSWORD WE CREATE IN NEXT STEP)
useSTARTTLS=YES

A note here... Some of these settings already exists such as the hostname. Do not make a new line entry for this, just check to be sure it has the proper data.



4.Lets test it

Sudo /etc/init.d/runFetchIP

Wait 45 seconds and check email, did it work?
YES - Good Job, reboot your Pi and wait for your IP !!

NO - You screwed something up, try this:

A. Reboot the Pi and try again
B. Look at ssmpt.conf again and be sure you set all the fields up
C. Are you connected to the internet?
D. You can open /var/log/mail.err to see what errors there are. Does it say anything about authentication? If so then your app password is not working(did you include the spaces -see step 1 note).
E. Google it!!


https://jeffrey_mitchell@bitbucket.org/jeffrey_mitchell/sendip.git