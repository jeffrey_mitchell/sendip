#!/bin/bash


script()
{

sleep 45

#The purpose of this script will be to grep the local IP and email results
EMAIL=ENTER_YOUR_EMAIL_HERE@gmail.com

ip=`ip addr | grep -Po '(?!(inet 127.\d.\d.1))(inet \K(\d{1,3}\.){3}\d{1,3})'`

#Get the name of the SSID we are currently connected to. Hopefully has outbound access
ssid=`iwgetid`

#What time is it
date=`date +"%H:%M:%S"`

#Send an email with our IP

echo "Hey there, your Pi just booted @ $date and has an IP of: $ip on SSID: $ssid" | mail -s "New IP:$ip" $EMAIL
}

script &
